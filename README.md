# Wolfterm

A very simple script to return wolfram api calls from the terminal. 

# Setup
Get a wolfram api key at https://developer.wolframalpha.com/portal/signup.html
You are limited to 2000 free calls a month or around 66 a day.

Replace "my_key" in config.yml with it.

Install dependencies with ```pip install -r requirements.txt```.

# Use

```
usage: wolfterm [-h] -q QUERY [-s]

optional arguments:
  -h, --help            show this help message and exit
  -q QUERY, --query QUERY
                        Takes in a string query
  -s, --short           Shortens the response to just the input and result

```